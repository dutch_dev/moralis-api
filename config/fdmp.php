<?php

return [
    'key' => env('MORALIS_KEY', ''),
    'url' => env('MORALIS_URL', 'https://deep-index.moralis.io/api/v2/'),
];
