<?php

namespace Fungible\Fdmp;

use Illuminate\Support\ServiceProvider;

class FdmpServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/fdmp.php' => config_path('fdmp.php')
        ]);
    }

    public function register()
    {
        $this->app->singleton(Moralis::class, function() {
            return new Moralis();
        });
    }
}
