<?php

namespace Fungible\Fdmp;

use GuzzleHttp\Client;
use App\DataTransferObjects\Token;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection;

class Fdmp extends Client
{
    public function __construct(array $config = [])
    {
        $url = config('fdmp.url');
        $key = config('fdmp.key');

        parent::__construct(array_merge($config, [
            'base_uri' => $url,
            'headers' => [
                'x-api-key' => $key,
            ],
        ]));
    }

/**
     * @param array $options
     * @return string
     */
    public function getQuery($options = []){
        $chain = 'eth';
        if(true === env('USE_TESTNET')){
            $chain = 'rinkeby';
        }
        $query = "?chain=" . $chain;

        foreach($options as $key => $value){
            if(null !== $value){
                $query .= "&" . $key . "=" . $value;
            }
        }

        return $query;
    }

    /**
     * @param string $wallet
     * @return array|null
     */
    public function getNfts(string $wallet, string $cursor = null)
    {
        $query = $this->getQuery([
            'format' => 'decimal',
            'cursor' => $cursor,
        ]);

        try {
            $response = $this->get($wallet . '/nft' . $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return null;
        }

        return $responseArray;
    }

    /**
     * @param string $contract
     * @return string|null
     */
    public function contractSync(string $contract)
    {
        try {
            $response = $this->put('nft/' . $contract . '/sync');
            $response = $response->getReasonPhrase();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return null;
        }

        return $response;
    }

    /**
     * @param string $contract
     * @param string $token
     * @return int|null
     */
    public function resyncNftMetadata(string $contract, string $token)
    {
        try {
            $response = $this->get('nft/' . $contract . '/' . $token . '/metadata/resync?chain=eth&flag=uri&mode=sync');
            $responseCode = $response->getStatusCode();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return null;
        }

        return $responseCode;
    }

    /**
     * @param string $wallet
     * @param array $tokenAddresses
     * @return Collection|null
     */
    public function getTokenBalance(string $wallet, array $tokenAddresses = null): ?Collection
    {
        try {
            if(null !== $tokenAddresses){
                $parameters = '?chain=eth';
                foreach($tokenAddresses as $tokenAddress){
                    $parameters .= '&token_addresses=' . $tokenAddress;
                }
                $response = $this->get($wallet . '/erc20' . $parameters);
            }else{
                $response = $this->get($wallet . '/erc20');
            }
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return null;
        }
        $tokenCollection = collect();
        if(count($responseArray) > 0){
            foreach($responseArray as $token){
                $tokenCollection->add(new Token($token));
            }
        }

        return $tokenCollection;
    }
}
